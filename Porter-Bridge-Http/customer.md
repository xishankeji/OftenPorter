### Java-WebSocket说明
使用版本
```xml
<dependency>
    <groupId>org.java-websocket</groupId>
    <artifactId>Java-WebSocket</artifactId>
    <version>1.5.2</version>
</dependency>
```

#### 修改说明
##### 对AbstractWebSocket的修改
1. 增加`enablePing`成员及setter与getter。
```java
private boolean enablePing = true;
```

- 在`restartConnectionLostTimer(){}`里`cancelConnectionLostTimer()`后加入：
```java
if(!enablePing){
    return;
}
```

2. 增加`pingTime`成员及setter与getter。
```java
private long pingTime=connectionLostTimeout/2;
```
```java
public void setPingTime(long pingTime)
{
    synchronized (syncConnectionLost) {
      this.pingTime = TimeUnit.SECONDS.toNanos(pingTime);
      onTimeoutChange();
    }
}
```

- 将`setConnectionLostTimeout(){}`处理部分移动到`onTimeoutChange(){}`;

- 将`restartConnectionLostTimer(){}`最后的
```java
connectionLostCheckerFuture = connectionLostCheckerService
        .scheduleAtFixedRate(connectionLostChecker, connectionLostTimeout, connectionLostTimeout,
            TimeUnit.NANOSECONDS);
```
改为
```java
connectionLostCheckerFuture = connectionLostCheckerService.scheduleAtFixedRate(connectionLostChecker, pingTime,pingTime,TimeUnit.NANOSECONDS);
```