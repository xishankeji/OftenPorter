package cn.xishan.oftenporter.porter.core.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by chenyg on 2017-04-18.
 */
public class PackageUtilTest {
    @Test
    public void testGetPackageWithRelative() {
        Class<?> clazz = Object.class;
        Assert.assertEquals("java.util.Set", PackageUtil.getPackageWithRelative(clazz, "../util/Set", '.'));
        Assert.assertEquals("java", PackageUtil.getPackageWithRelative(clazz, "..", '.'));

        LogUtil.printPosLn(PackageUtil.getPathWithRelative('.', clazz.getName(), false, "../util/Set", '.'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "/mybatis/", null, "test.xml", '/'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "mybatis/", null, "test.xml", '/'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "/mybatis", null, "test.xml", '/'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "/mybatis", true, "test.xml", '/'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "/mybatis", true, "/test.xml", '.'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative('/', "/mybatis", true, "/test.xml", '/'));
        LogUtil.printPosLn(PackageUtil.getPathWithRelative("/", "./"));
    }

    @Test
    public void testUrl() {
        Assert.assertEquals("http://localhost:8080/a/b/c.txt",
                PackageUtil.getPathWithRelative("http://localhost:8080/a/b/x.txt", "./c.txt"));
        Assert.assertEquals("http://localhost:8080/a/c.txt",
                PackageUtil.getPathWithRelative("http://localhost:8080/a/b/x.txt", "../c.txt"));
        Assert.assertEquals("http://localhost:8080/a/b/d/c.txt",
                PackageUtil.getPathWithRelative("http://localhost:8080/a/b/x.txt", "./d/c.txt"));
    }

}
