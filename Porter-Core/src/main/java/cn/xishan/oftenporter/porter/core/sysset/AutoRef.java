package cn.xishan.oftenporter.porter.core.sysset;

/**
 * 见：{@linkplain IAutoSetter#setRef(String, Object)}。
 *
 * @author Created by https://github.com/CLovinr on 2021/8/4.
 */
public interface AutoRef<T> {

    interface Listener<T> {
        void change(T newObj, T oldObj);
    }



    T get();

    Class<T> type();

    void onChange(Listener<T> listener);

    /**
     * 第一次获取到非空值时回调。
     *
     * @param listener
     */
    void onFirst(Listener<T> listener);
}
