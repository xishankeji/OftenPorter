package cn.xishan.oftenporter.oftendb.util;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Created by https://github.com/CLovinr on 2019/3/28.
 */
public class SimpleSqlUtilTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSqlUtilTest.class);

    @Test
    public void test1() {
        SimpleSqlUtil simpleSqlUtil = new SimpleSqlUtil();
        SimpleSqlUtil.SQLPart sqlPart = simpleSqlUtil.fromNameValues(null, null, null,
                "$or[", "$true", "$false", "$or]");
        LOGGER.debug("where:{}", sqlPart.nowhere);
        Assert.assertEquals("(TRUE OR FALSE )", sqlPart.nowhere);

        sqlPart = simpleSqlUtil.fromNameValues(null, null, null,
                "$not[",
                "$or[", "a", 1, "a", 2, "$or]",
                "$or[", "b", 1, "b", 2, "$or]",
                "$not]");
        LOGGER.debug("where:{}", sqlPart.nowhere);
        Assert.assertEquals("NOT ((`a` = #{query.__a__2} OR `a` = #{query.__a__3} )AND (`b` = #{query.__b__6} OR `b` = #{query.__b__7} ))", sqlPart.nowhere);

        sqlPart = simpleSqlUtil.fromNameValues(null, null, null,
                "$not-or[",
                "a", 1, "a", 2,
                "$not-or]");
        LOGGER.debug("where:{}", sqlPart.nowhere);
        Assert.assertEquals("NOT (`a` = #{query.__a__1} OR `a` = #{query.__a__2} )", sqlPart.nowhere);

    }
}